"░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
"
" Colors
"
"░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░

" Automatically use base16 color scheme from shell
if filereadable(expand("~/.vimrc_background"))
	let base16colorspace=256
	source ~/.vimrc_background
else
	set background=dark
endif

set t_Co=256


"░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
"
" Key Bindings
"
"░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░

" Accomodate for sticky shifting
command W w

" Clear Search Results
nnoremap <CR> :noh<CR><CR>

" Set F2 to prevent indenting for pasting large text blocks
set pastetoggle=<F2>

" Hide/show line numbers
nnoremap <F3> :set invnumber invrelativenumber<CR>

" Spell checking
map <F4> <Esc>:setlocal spell spelllang=en_us
map <S-F4> <Esc>:setlocal nospell


"░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
"
" File configurations
"
"░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░

set ff=unix
set encoding=utf-8
set fileencoding=utf-8

" Enable modeline support
set modeline


"░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
"
" Interface Behavior
"
"░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░

" Set a scroll offset, so page keys don't leave you hanging at 1 line. Also,
" jump up/down when the cursor leaves the page
set scrolloff=5
set scrolljump=5

" Set up tab width and line length
set tabstop=4
set shiftwidth=4
set textwidth=80
set autoindent

" Nice improvements
set showcmd		" Show (partial) command in status line.
set showmatch	" Show matching brackets.
set ignorecase	" Do case insensitive matching
set smartcase	" Do smart case matching

" Make sure relative line numbers are shown
set number relativenumber

" Highlight Search Terms
set hlsearch

" Set up the statusbar to show always, and include file encoding
set laststatus=2



"░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
"
" Plugins
"
"░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░

" Auto-indentation plugin
if has("autocmd")
  filetype plugin indent on
endif


" -----------------
" minimap
"let g:minimap_highlight='Visual'

" Use autocmd to open the minimap on startup
"autocmd VimEnter * normal \mm

" Default shortcut commands
let g:minimap_show='<leader>ms'
let g:minimap_update='<leader>mu'
let g:minimap_close='<leader>gc'
"let g:minimap_toggle='<leader>gt'

" Easy minimap toggle
let g:minimap_toggle='<F2>'
" -----------------


" -----------------
" minibufexpl - Buffer Plugin
" Unlock the true potential of minibufexpl!
let g:miniBufExplMapWindowNavVim = 1
let g:miniBufExplMapWindowNavArrows = 1
let g:miniBufExplMapCTabSwitchBufs = 1
let g:miniBufExplModSelTarget = 1 

" Split vertically
let g:miniBufExplVSplit = 30

" Always open minibufexplorer window
let g:miniBufExplBuffersNeeded = 2

" Make sure syntax hilighting is on
let g:miniBufExplForceSyntaxEnable = 1
